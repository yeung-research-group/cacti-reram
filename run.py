import sys
import os

if sys.version_info[0] < 3:
    import ConfigParser
    ini_config = ConfigParser.RawConfigParser()
else:
    import configparser
    ini_config = configparser.RawConfigParser()

#Sweep through the different configurations of banks and subbanks
def subbank_sweep( subbanks ):
    global ini_config
    bit_line_div = 2 * subbanks
    banks = 1
    
    ini_config.set('modeling','force_config', True)
    ini_config.set('cache_sizing','sectored_cache', True)
    ini_config.set('reram_settings','integrate', True)
    ini_config.set('reram_settings','sep_bus', False)
    
    while bit_line_div > 1 :
        ini_config.set('modeling','Ndbl', bit_line_div)
        ini_config.set('cache_sizing','UCA_banks', banks)
        banks_col = banks
        
        while banks_col >= 1 :
            ini_config.set('cache_sizing','banks_per_column', banks_col)
            
            with open("autorun.ini", 'w') as run_config:
                ini_config.write(run_config)
            
            os.system('./cacti -infile autorun.ini')
            
            banks_col /= 2
            
        bit_line_div /= 2
        banks *= 2
            
    
#Sweep through the different steps for ReRAM integration    
def ideal_sweep():
    global ini_config
    
    ini_config.set('modeling','force_config', False)   
    ini_config.set('cache_sizing','sectored_cache', False)
    ini_config.set('reram_settings','integrate', False)
    ini_config.set('reram_settings','sep_bus', False)
    ini_config.set('reram_settings','directory_bus', False)
    
    #default
    with open("autorun.ini", 'w') as run_config:
                ini_config.write(run_config)
    os.system('./cacti -infile autorun.ini')
    
    #sectoring
    ini_config.set('cache_sizing','sectored_cache', True)
    with open("autorun.ini", 'w') as run_config:
                ini_config.write(run_config)
    os.system('./cacti -infile autorun.ini')
    
    #mat size
    ini_config.set('modeling','force_config', True)
    with open("autorun.ini", 'w') as run_config:
                ini_config.write(run_config)
    os.system('./cacti -infile autorun.ini')
    
    #add reram
    ini_config.set('reram_settings','integrate', True)
    with open("autorun.ini", 'w') as run_config:
        ini_config.write(run_config)
    os.system('./cacti -infile autorun.ini')
    
    #add bus
    ini_config.set('reram_settings','sep_bus', True)
    with open("autorun.ini", 'w') as run_config:
        ini_config.write(run_config)
    os.system('./cacti -infile autorun.ini')
    
    run_config.close()
 

#Sweep through the different tech nodes
def tech_sweep():
    global ini_config

    tech_nodes = [0.022, 0.032, 0.045, 0.065, 0.090]

    for tn in tech_nodes :
        ini_config.set('modeling','technology', tn)
        with open("autorun.ini", 'w') as run_config:
                    ini_config.write(run_config)
        os.system('./cacti -infile autorun.ini')

    run_config.close()
 
    
#Sweep through the different steps for ReRAM integration
def ideal_tech_sweep():
    global ini_config

    tech_nodes = [0.022, 0.032, 0.045, 0.065, 0.090]

    ini_config.set('modeling','force_config', False)
    ini_config.set('cache_sizing','sectored_cache', False)
    ini_config.set('reram_settings','integrate', False)
    ini_config.set('reram_settings','sep_bus', False)
    ini_config.set('reram_settings','directory_bus', False)

    for tn in tech_nodes :
        ini_config.set('modeling','technology', tn)
        ini_config.set('modeling','force_config', False)
        ini_config.set('cache_sizing','sectored_cache', False)
        ini_config.set('reram_settings','integrate', False)
        ini_config.set('reram_settings','sep_bus', False)
        ini_config.set('reram_settings','directory_bus', False)

        #default
        with open("autorun.ini", 'w') as run_config:
                    ini_config.write(run_config)
        os.system('./cacti -infile autorun.ini')

        #sectoring
        ini_config.set('cache_sizing','sectored_cache', True)
        with open("autorun.ini", 'w') as run_config:
                    ini_config.write(run_config)
        os.system('./cacti -infile autorun.ini')
        
        #mat size
        ini_config.set('modeling','force_config', True)
        with open("autorun.ini", 'w') as run_config:
                    ini_config.write(run_config)
        os.system('./cacti -infile autorun.ini')

        #add reram
        ini_config.set('reram_settings','integrate', True)
        with open("autorun.ini", 'w') as run_config:
            ini_config.write(run_config)
        os.system('./cacti -infile autorun.ini')

        #add bus
        ini_config.set('reram_settings','sep_bus', True)
        with open("autorun.ini", 'w') as run_config:
            ini_config.write(run_config)
        os.system('./cacti -infile autorun.ini')

    run_config.close()


#Global Scope 
base_ini_config = open("reram_cache.ini", 'r')

ini_config.optionxform = str
ini_config.readfp(base_ini_config)
base_ini_config.close()

#Base settings
ini_config.set('cache_sizing','size', 2097152)
ini_config.set('cache_sizing','block_size', 64)
ini_config.set('cache_sizing','associativity', 8)
ini_config.set('cache_sizing','UCA_banks', 1)
ini_config.set('cache_sizing','banks_per_column', -1)
ini_config.set('cache_sizing','bus_width', 64)
ini_config.set('modeling','technology', 0.032)

ini_config.set('modeling','force_config', False)
ini_config.set('cache_sizing','sectored_cache', False)
ini_config.set('reram_settings','integrate', False)
ini_config.set('reram_settings','sep_bus', False)
ini_config.set('reram_settings','directory_bus', False)

ini_config.set('cell_types','data_array_cell_type', 'itrs-lstp')
ini_config.set('cell_types','data_array_peripheral_type', 'itrs-lstp')
ini_config.set('cell_types','tag_array_cell_type', 'itrs-lstp')
ini_config.set('cell_types','tag_array_peripheral_type', 'itrs-lstp')


# 2 1k x 1k ReRAM arrays / mat
subbanks = 128
ini_config.set('cache_sizing','UCA_banks', 4) #update
ini_config.set('cache_sizing','banks_per_column', 1)
ini_config.set('modeling','Ndwl', 16)
ini_config.set('modeling','Ndbl', 64) #update
ini_config.set('modeling','Nspd', 0.5)
ini_config.set('reram_settings','array_x', 1024)
ini_config.set('reram_settings','array_y', 1024)
ini_config.set('reram_settings','arrays_per_mat', 2)

#subbank_sweep( subbanks )
#ideal_sweep()

# 4 1k x 1k ReRAM arrays / mat
subbanks = 32
ini_config.set('cache_sizing','UCA_banks', 4)
ini_config.set('cache_sizing','banks_per_column', 1)
ini_config.set('modeling','Ndwl', 16)
ini_config.set('modeling','Ndbl', 16)
ini_config.set('modeling','Nspd', 0.5)
ini_config.set('reram_settings','array_x', 1024)
ini_config.set('reram_settings','array_y', 1024)
ini_config.set('reram_settings','arrays_per_mat', 4)
#ideal_sweep()

ini_config.set('cell_types','data_array_cell_type', 'itrs-lstp')
ini_config.set('cell_types','data_array_peripheral_type', 'itrs-lstp')
ini_config.set('cell_types','tag_array_cell_type', 'itrs-lstp')
ini_config.set('cell_types','tag_array_peripheral_type', 'itrs-lstp')
ideal_tech_sweep()

ini_config.set('cell_types','data_array_cell_type', 'itrs-lop')
ini_config.set('cell_types','data_array_peripheral_type', 'itrs-lop')
ini_config.set('cell_types','tag_array_cell_type', 'itrs-lop')
ini_config.set('cell_types','tag_array_peripheral_type', 'itrs-lop')
ideal_tech_sweep()

# 2 2k x 2k ReRAM arrays / mat
subbanks = 16
ini_config.set('cache_sizing','UCA_banks', 2)
ini_config.set('cache_sizing','banks_per_column', 1)
ini_config.set('modeling','Ndwl', 16)
ini_config.set('modeling','Ndbl', 16)
ini_config.set('modeling','Nspd', 1)
ini_config.set('reram_settings','array_x', 2048)
ini_config.set('reram_settings','array_y', 2048)
ini_config.set('reram_settings','arrays_per_mat', 2)
#ideal_sweep()

ini_config.set('cell_types','data_array_cell_type', 'itrs-lstp')
ini_config.set('cell_types','data_array_peripheral_type', 'itrs-lstp')
ini_config.set('cell_types','tag_array_cell_type', 'itrs-lstp')
ini_config.set('cell_types','tag_array_peripheral_type', 'itrs-lstp')
ideal_tech_sweep()

ini_config.set('cell_types','data_array_cell_type', 'itrs-lop')
ini_config.set('cell_types','data_array_peripheral_type', 'itrs-lop')
ini_config.set('cell_types','tag_array_cell_type', 'itrs-lop')
ini_config.set('cell_types','tag_array_peripheral_type', 'itrs-lop')
ideal_tech_sweep()

# 4 2k x 2k ReRAM arrays / mat
subbanks = 8
ini_config.set('cache_sizing','UCA_banks', 1) #update
ini_config.set('cache_sizing','banks_per_column', 1)
ini_config.set('modeling','Ndwl', 16)
ini_config.set('modeling','Ndbl', 16) #update
ini_config.set('modeling','Nspd', 1)
ini_config.set('reram_settings','array_x', 2048)
ini_config.set('reram_settings','array_y', 2048)
ini_config.set('reram_settings','arrays_per_mat', 4)

#subbank_sweep( subbanks )
#ideal_sweep()
    
