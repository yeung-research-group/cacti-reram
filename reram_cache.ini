[general]
# Type of memory - cache (with a tag array) or ram (scratch ram similar to a register file) 
# or main memory (no tag array and every access will happen at a page granularity Ref: CACTI 5.3 report)
cache_type = cache
;cache_type = ram
;cache_type = main memory

;add_ECC = true

;print_level = CONCISE  ;(DETAILED, CONCISE)
print_level = DETAILED ;(DETAILED, CONCISE)

# for debugging
print_input_parameters = true
;print_input_parameters = false

[cache_sizing]
# Cache size
#size in bytes
#size = 2048
#size = 4096
#size = 16384
#size = 32768
#size = 65536
#size = 131072
#size = 262144
size = 1048576
#size = 2097152
#size = 4194304
#size = 8388608
#size = 16777216
#size = 33554432
#size = 134217728
#size = 67108864
#size = 1073741824

# Line size in bytes
#block_size = 8
block_size = 64

# To model Fully Associative cache, set associativity to zero
#associativity = 0
#associativity = 2
#associativity = 4
associativity = 8
#associativity = 16

#NUCA or UCA model
cache_model = UCA
#cache_model = NUCA

# In order for CACTI to find the optimal NUCA bank value the following
# variable should be assigned 0.
NUCA_banks = 16

# Multiple banks connected using a bus
UCA_banks = 16
#Force the number of banks per column (-1 will find optimal configuration)
banks_per_column = -1

# Bus width include data bits and address bits required by the decoder
bus_width = 64
#bus_width = 512

# to model special structure like branch target buffers, directory, etc. 
# change the tag size parameter in bits
# if you want cacti to calculate the tagbits, set the tag size to "default"
tag_size = default
#tag_size = 22

#add extra valid and dirty bits for sectoring
sectored_cache = true


#ReRAM settings
[reram_settings]
#Should ReRAM be integrated into the cache model
integrate = true
#Do the ReRAM and cache require separate interconnects
sep_bus = true
#Do we additionally want to include a directory interconnect
directory_bus = false
#Size (in bits) of the ReRAM array (512, 1024, 2048, 4096)
array_x = 1024
array_y = 1024
#How many ReRAM arrays surround a single mat (2, 4)
arrays_per_mat = 4
#How the ReRAM arrays should be sized relative to cache mat 
#    DEFINED - use user defined parameters,
#    CLOSEST - minimize lack of overlap in either direction,
#    OVERFIT - minimize cache uncovered by ReRAM arrays (larger ReRAM arrays),
#    UNDERFIT - minimize empty area under ReRAM arrays (smaller ReRAM arrays)
fit_goal = DEFINED

[modeling]
# force CACTI to model the cache with the 
# following Ndbl, Ndwl, Nspd, Ndsam,
# and Ndcm values
force_config = true
#force_config = false
Ndwl = 16
Ndbl = 2
Nspd = 0.5
Ndcm = 2
Ndsam1 = 0
Ndsam2 = 0
#Technology node in micrometers
technology = 0.032
#technology = 0.045
#technology = 0.065
#technology = 0.090

; 300-400 in steps of 10 (K)
operating_temperature = 360

# fast - data and tag access happen in parallel
# sequential - data array is accessed after accessing the tag array
# normal - data array lookup and tag access happen in parallel
#          final data block is broadcasted in data array h-tree 
#          after getting the signal from the tag array
;access_mode = fast
access_mode = normal
;access_mode = sequential


# DESIGN OBJECTIVE for UCA (or banks in NUCA)
design_objective = 0:0:0:100:0 ; (weight delay, dynamic power, leakage power, cycle time, area)

# Percentage deviation from the minimum value 
# Ex: A deviation value of 10:1000:1000:1000:1000 will try to find an organization
# that compromises at most 10% delay. 
# NOTE: Try reasonable values for % deviation. Inconsistent deviation
# percentage values will not produce any valid organizations. For example,
# 0:0:100:100:100 will try to identify an organization that has both
# least delay and dynamic power. Since such an organization is not possible, CACTI will
# throw an error. Refer CACTI-6 Technical report for more details
deviate = 20:100000:100000:100000:100000 ;(delay, dynamic power, leakage power, cycle time, area)

# Objective for NUCA
NUCAdesign_objective = 100:100:0:0:100 ;(weight delay, dynamic power, leakage power, cycle time, area)
NUCAdeviate = 10:10000:10000:10000:10000 ;(delay, dynamic power, leakage power, cycle time, area)

# Set optimize tag to ED or ED^2 to obtain a cache configuration optimized for
# energy-delay or energy-delay sq. product
# Note: Optimize tag will disable weight or deviate values mentioned above
# Set it to NONE to let weight and deviate values determine the 
# appropriate cache configuration (ED, ED^2, NONE)
;optimize = ED
optimize = ED^2
;optimize = NONE

# power gating
[power_gating]
array = false
WL = false
CL = false
bitline_floating = false
interconnect = false
performance_loss = 0.01
CLDriver_vertical = false

[ports]
read_write_port = 1
exclusive_read_port = 0
exclusive_write_port = 0
single_ended_read_ports = 0


# following three parameters are meaningful only for main memories
[main_memory]
#page size in bits
page_size = 8192 
burst_length = 8
internal_prefetch_width = 8

[cell_types]
# following parameter can have one of five values -- (itrs-hp, itrs-lstp, itrs-lop, lp-dram, comm-dram)
data_array_cell_type = itrs-hp
#data_array_cell_type = itrs-lstp
#data_array_cell_type = itrs-lop
# following parameter can have one of three values -- (itrs-hp, itrs-lstp, itrs-lop)
data_array_peripheral_type = itrs-hp
#data_array_peripheral_type = itrs-lstp
#data_array_peripheral_type = itrs-lop
# following parameter can have one of five values -- (itrs-hp, itrs-lstp, itrs-lop, lp-dram, comm-dram)
tag_array_cell_type = itrs-hp
#tag_array_cell_type = itrs-lstp
#tag_array_cell_type = itrs-lop
# following parameter can have one of three values -- (itrs-hp, itrs-lstp, itrs-lop)
tag_array_peripheral_type = itrs-hp
#tag_array_peripheral_type = itrs-lstp
#tag_array_peripheral_type = itrs-lop


[network]
# NOTE: for nuca network frequency is set to a default value of 
# 5GHz in time.c. CACTI automatically
# calculates the maximum possible frequency and downgrades this value if necessary

# By default CACTI considers both full-swing and low-swing 
# wires to find an optimal configuration. However, it is possible to 
# restrict the search space by changing the signaling from "default" to 
# "fullswing" or "lowswing" type. (fullswing, lowswing, default)
wire_signaling = Global_30
;wire_signaling = default
;wire_signaling = lowswing

;wire_inside_mat = global
wire_inside_mat = semi-global
;wire_outside_mat = global
wire_outside_mat = semi-global

interconnect_projection = conservative
;interconnect_projection = aggressive

# Contention in network (which is a function of core count and cache level) is one of
# the critical factor used for deciding the optimal bank count value
# core count can be 4, 8, or 16
;core_count = 4
;core_count = 8
core_count = 16
cache_level  = L3 ;(L2/L3)


#### Default CONFIGURATION values for baseline external IO parameters to DRAM. More details can be found in the CACTI-IO technical report (), especially Chapters 2 and 3.

[dram]
# Memory Type (D3=DDR3, D4=DDR4, L=LPDDR2, W=WideIO, S=Serial). Additional memory types can be defined by the user in extio_technology.cc, along with their technology and configuration parameters.
dram_type = DDR3
#dram_type = DDR4
#dram_type = LPDDR2
#dram_type = WideIO
#dram_type = Serial

# Memory State (Read, Write, Idle or Sleep) 
#io_state = READ
io_state = WRITE
#io_state = IDLE
#io_state = SLEEP

#Address bus timing. To alleviate the timing on the command and address bus due to high loading (shared across all memories on the channel), the interface allows for multi-cycle timing options. 
#addr_timing = 0.5 ;DDR
addr_timing = 1.0  ;SDR (half of DQ rate)
#addr_timing = 2.0 ;2T timing (One fourth of DQ rate)
#addr_timing = 3.0 ;3T timing (One sixth of DQ rate)

# Memory Density (Gbit per memory/DRAM die)
mem_density = 4 ;Gb Valid values 2^n Gb

# IO frequency (MHz) (frequency of the external memory interface).
# As of current memory standards (2013), valid range 0 to 1.5 GHz for DDR3, 0 to 533 MHz for LPDDR2,
# 0 - 800 MHz for WideIO and 0 - 3 GHz for Low-swing differential. However this can change, and the user is
# free to define valid ranges based on new memory types or extending beyond existing standards for existing dram types.
bus_freq = 800 ;MHz 

# Duty Cycle (fraction of time in the Memory State defined above)
duty_cycle = 1.0 ;Valid range 0 to 1.0

# Activity factor for Data (0->1 transitions) per cycle (for DDR, need to account for the higher activity in this parameter. E.g. max. activity factor for DDR is 1.0, for SDR is 0.5)
activity_dq = 1.0 ;Valid range 0 to 1.0 for DDR, 0 to 0.5 for SDR

# Activity factor for Control/Address (0->1 transitions) per cycle (for DDR, need to account for the higher activity in this parameter. E.g. max. activity factor for DDR is 1.0, for SDR is 0.5)
activity_ca = 0.5 ;Valid range 0 to 1.0 for DDR, 0 to 0.5 for SDR, 0 to 0.25 for 2T, and 0 to 0.17 for 3T

# Number of DQ pins 
num_dq = 72 ; Number of DQ pins. Includes ECC pins.

# Number of DQS pins. DQS is a data strobe that is sent along with a small number of data-lanes so the source synchronous timing is local to these DQ bits.
# Typically, 1 DQS per byte (8 DQ bits) is used. The DQS is also typucally differential, just like the CLK pin. 
# 2 x differential pairs. Include ECC pins as well. Valid range 0 to 18. For x4 memories, could have 36 DQS pins.
num_dqs = 18 

# Number of CA pins 
num_ca = 25 ;Valid range 0 to 35 pins.

# Number of CLK pins. CLK is typically a differential pair. In some cases additional CLK pairs may be used to limit the loading on the CLK pin. 
num_clk = 2 ; 2 x differential pair. Valid values: 0/2/4.

# Number of Physical Ranks
num_mem_dq = 2 ;Number of ranks (loads on DQ and DQS) per buffer/register. If multiple LRDIMMs or buffer chips exist, the analysis for capacity and power is reported per buffer/register. 

# Width of the Memory Data Bus
mem_data_width = 8 ; x4 or x8 or x16 or x32 memories. For WideIO upto x128.

# RTT Termination Resistance
rtt_value = 10000

# RON Termination Resistance
ron_value = 34

# Time of flight for DQ
#tflight_value =

# Parameter related to MemCAD
[memcad]
# Number of BoBs: 1,2,3,4,5,6,
num_bobs = 1
	
# Memory System Capacity in GB
capacity = 80	
	
# Number of Channel per BoB: 1,2. 
num_channels_per_bob = 1	

# First Metric for ordering different design points	
first_metric = Cost
#first_metric = Bandwidth
#first_metric = Energy
	
# Second Metric for ordering different design points	
#second_metric = Cost
second_metric = Bandwidth
#second_metric = Energy

# Third Metric for ordering different design points	
#third_metric = Cost
#third_metric = Bandwidth
third_metric = Energy
	
	
# Possible DIMM option to consider
#DIMM_model = JUST_UDIMM
#DIMM_model = JUST_RDIMM
#DIMM_model = JUST_LRDIMM
DIMM_model = ALL

#if channels of each bob have the same configurations
#mirror_in_bob = true
mirror_in_bob = false

#if we want to see all channels/bobs/memory configurations explored	
#verbose = true
verbose = false

[3Ddram]
## Parameters for 3D DRAM
burst_depth = 8
IO_width = 4
system_frequency = 533 ;(MHz)

stacked_die_count = 1
partitioning_granularity = 0 ; 0: coarse-grained rank-level; 1: fine-grained rank-level
TSV_projection = 1 ; 0: ITRS aggressive; 1: industrial conservative
